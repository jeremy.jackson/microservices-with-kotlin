package com.microservices.chapter3

// class SimpleObject {
//   public val name = "hello"
//   private val zone = "world"
//   public fun getPlace() = zone
// }

data class SimpleObject(var name: String = "hello", var place: String = "world")
