package com.microservices.chapter3

import java.util.concurrent.ConcurrentHashMap
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class Chapter3Application

fun main(args: Array<String>) {
	runApplication<Chapter3Application>(*args)
}
